<?php

namespace Drupal\Tests\commerce_custom_checkout_message\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for commerce_custom_checkout_message.
 *
 * @group commerce_custom_checkout_message
 */
class CommerceCustomCheckoutMessageGenericTest extends GenericModuleTestBase {

  /**
   * {@inheritDoc}
   */
  protected function assertHookHelp(string $module): void {
    // Don't do anything here. Just overwrite this useless method, so we do
    // don't have to implement hook_help().
  }

}
